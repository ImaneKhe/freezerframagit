CREATE DATABASE FreezerDb;
CREATE TABLE if not exists FreezerDb.Music (Music_ID INT NOT NULL AUTO_INCREMENT,Title varchar(255),Artist varchar(255), User varchar(255), PRIMARY KEY (Music_ID));
CREATE TABLE if not exists FreezerDb.Auth (ID INT NOT NULL AUTO_INCREMENT,User varchar(255), Passwd varchar(255),PRIMARY KEY (ID));

INSERT INTO FreezerDb.Auth  (User,Passwd) VALUES('admin', 'admin');
INSERT INTO FreezerDb.Auth  (User,Passwd) VALUES('imane', 'imane');
INSERT INTO FreezerDb.Auth  (User,Passwd) VALUES('guest', 'guest');

INSERT INTO FreezerDb.Music  (Title,Artist,User) VALUES('Hello' , 'Adele', 'hassan');
INSERT INTO FreezerDb.Music  (Title,Artist,User) VALUES ('Boulbi' , 'Booba ', 'jfk');
INSERT INTO FreezerDb.Music  (Title,Artist,User) VALUES ('Good Life' , 'Kanye West ', 'imane');
