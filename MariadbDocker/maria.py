import mariadb

class Music_db():
    def __init__(self) -> None:
        pass

    def db_connect(self):
        self.db_create()
        try:
            self.conn = mariadb.connect(
                user="Freezer",
                password="freezer",
                host="localhost",
                port=3306,
                database="FreezerDb"
            )
        except mariadb.Error as e:
            print(f"Error connecting to MariaDB Platform: {e}")
        cur.execute("CREATE TABLE IF NOT EXISTS Music (Music_ID int auto_increment, Artist varchar(255),Song varchar(255),User varchar(255), primary key Music_ID) ")
        

    def db_insert(self,Artist,Song,User):
        self.db_connect()
        cur = self.conn.cursor()
        statement = "INSERT INTO Music (Artist,Song,User) VALUES (%s,%s,%s);"
        data = (str(Artist),str(Song),str(User))
        cur.execute(statement, data)
        print('Insertion Complete !')
    
    def db_search(self,Artist,Song,User):
        self.db_connect()
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM Music WHERE Artist LIKE ? HAVING User = ? ",(Artist,Song,User))

    def db_GetAll(self):
        self.db_connect()
        self.cur = self.conn.cursor()
        #self.cur.execute("SELECT * FROM Music ",(User))
        self.cur.execute("SELECT * FROM Music ")
        for line in self.cur:
            print(line)


    def db_create(self):
        self.conn = mariadb.connect(
            user="root",
            password="root",
            port=3306,
            host="localhost")
        cur = self.conn.cursor()
        cur.execute("CREATE USER IF NOT EXISTS Freezer IDENTIFIED BY 'freezer';")
        cur.execute("CREATE DATABASE IF NOT EXISTS FreezerDb; ")
        cur.execute("grant all privileges on FreezerDb.* TO 'Freezer'@'localhost' identified by 'freezer';")
        cur.execute("flush privileges;")

Maria = Music_db()
Maria.db_GetAll()